from datetime import datetime, timedelta

ITSO_CODES = {
    "0005": "Create IPE",
    "0207": "Create IPE",
    "0208": "Amend IPE",
    "0209": "Entry",
    "0210": "Entry/Exit",
    "0311": "Hotlist Match",
    "0312": "Actionlist Match",
    "0400": "Unsuccesful",
    "0803": "Subscription",
    "0901": "ACK Class 1",
    "0902": "ACK Class 2",
    "0911": "NACK Class 1",
    "0912": "NACK Class 2"

}


STATION_LIST = {
    "5545": "London Victoria",
    "5268": "Falmer"
}

ITSO_BASE_DATE = datetime(1997,1,1,0,0,0)

def DTS_to_STR(DTS_date):
    # converts DTS date into a string
    int_date_minutes = int(DTS_date, 16)
    new_date = ITSO_BASE_DATE + timedelta(minutes=int_date_minutes)
    strdate = str(new_date)
    return strdate
