import xml.etree.ElementTree as EL
from misc import *
import json
import os
from prettytable import PrettyTable
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import FileSystemEventHandler

# TODO: if file is empty it should write an empty JSON object ... ?
# TODO: 0902 and other ACKS
# TODO: exception when new folder is created
# TODO: Add logging into a file + debug  logging if it crashes ?..


class MyClass(FileSystemEventHandler):

    def on_created(self, event):
        super(MyClass, self).on_created(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info("Created %s: %s", what, event.src_path)
        # Wait for it to finish writing to the file
        time.sleep(1)
        os.path.exists(event.src_path)
        # Why i need to call above line ?...

        file = EL.parse(event.src_path)
        root = file.getroot()
        print("Number of records:", len(root[1][1]))


        for i in range(0, len(root[1][1]), 1):

            global NLC
            global ITSO_CARD

            # Now depending on message we do something different
            ITSO_MSG_CODE = root[1][1][i][0].text

            desc = ITSO_CODES.get(ITSO_MSG_CODE)
            ITSO_DTS = root[1][1][i][1].text
            ITSO_ORIGINATOR = root[1][0][1].text

            # get number of destinations
            ITSO_Dest_Count = int(root[1][1][i][2][2].text)
            ITSO_DATA = root[1][1][i][2][ITSO_Dest_Count + 3].text.split(",")

            # If subscription then add to the list
            if ITSO_MSG_CODE == "0803":
                # TODO: extract the ITSO_Originator and the NLC (plinth)
                # TODO: CHeck spec for different messages
                # TODO: do i need separate cases for each message ?

                #NLC = parse_nlc(ITSO_DATA[5][:8])
                #plinth = ITSO_DATA[5][12:14]
                #print(plinth)
                subscribed_stations[root[1][0][1].text] = parse_nlc(ITSO_DATA[5][:8])
                write_subscribed_station()

            if ITSO_MSG_CODE == "0902":
                NLC = ITSO_DATA[0]
                ITSO_CARD = ITSO_DATA[0]

            elif ITSO_MSG_CODE == "0901":
                NLC = ITSO_DATA[0]
                ITSO_CARD = ITSO_DATA[0]

            else:
                NLC = parse_nlc(ITSO_DATA[5][:8])
                ITSO_CARD = ITSO_DATA[8]

            try:
                t.add_row([ITSO_MSG_CODE,
                                desc,
                                DTS_to_STR(ITSO_DTS),
                                ITSO_CARD,
                                ITSO_ORIGINATOR[-8:],
                                "1212",
                                subscribed_stations[ITSO_ORIGINATOR],  # NLC
                                STATION_LIST[subscribed_stations[ITSO_ORIGINATOR]]]  # NLC Name
                               )
                print("\n".join(t.get_string().splitlines()[-2:-1]))

            except KeyError as e:
                t.add_row([ITSO_MSG_CODE,
                                desc,
                                DTS_to_STR(ITSO_DTS),
                                ITSO_DATA[8],
                                "N/A",
                                "1212",
                                "N/A",
                                "N/A"]
                               )
                print("\n".join(t.get_string().splitlines()[-2:-1]))


def file_is_empty(path):
    return os.stat(path).st_size == 0


def parse_nlc(var):
    # Takes 35363537 an returns 5657 as NLC has '3' padding
    new=""
    for i, char in enumerate(var):
        if i % 2 == 1:
            new += char
    return new


def read_subscribed_stations():
    with open("stations.txt",'r') as f:
        try:
            if not file_is_empty("stations.txt"):
                try:
                    #print("file not empty")
                    return json.load(f)
                except ValueError:
                    print("File empty")
        except IOError:
            print("Error: can\'t find file or read data")


def write_subscribed_station():
    with open("stations.txt", 'w') as f:
        json.dump(subscribed_stations, f)


def print_message_body(object):
    print("Printing ITSO DATA FRAME Objects\n")
    for child in object:
        print(child.tag, child.text)
    # print(child.items)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = "C:\prototyping\ley3"
    # TOCDGC \\10.26.42.64\d$\itsofiles\HopsSender\Success
    # "C:\prototyping\ley3"

    # Read station already subscribed - ie received 0803 message or manually entered
    subscribed_stations = dict()
    print(subscribed_stations)
    subscribed_stations = read_subscribed_stations()

    event_handler = MyClass()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    print("Starting logging from NOW <TIME>\n")

    t = PrettyTable(["ITSO MSG CODE",
                     "Desc               ",
                     "ITSO DTS             ",
                     "ITSO CARD      ",
                     "ISAM ID ",
                     "RTD ID  ",
                     "NLC",
                     "NLC NAME          "])
    t.hrules = 1
    print(t)

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()